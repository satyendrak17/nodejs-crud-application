module.exports = function(router) {
    //localhost:300/books
    router.get('/', function(req, res) {
        res.send("//localhost:300/books");
    });
    //localhost:300/books/
    router.post('/', function(req, res) {
        res.send("Default Post call");
    });
    router.get('/myBoook', function(req, res){
        res.send("Get My book");
    });
}