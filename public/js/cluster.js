var cluster = require('cluster');

if(cluster.isMaster) {
    // Do master role actions
    // Get the cors and fork them.
    var cpuCount = require('os').cpus().length;
    console.log("number of workers -- ", cpuCount);

    // Fork all workers
    for(var indx = 0 ;  indx < cpuCount; indx++) {
        cluster.fork();
    }
    // Check if cluster is iive
    cluster.on('online', function(worker) {
        console.log("Worker with process id "+ worker.process.pid + ' is live');
    });
    // When a worker died
    cluster.on('exit', function(worker, code, signal) {
        console.log("Cluster of process id " + worker.process.pid + "died with code " +
         code + "and signal " + signal);
         cluster.fork(); // start a new worker to keep server alive
    });
} else {
    var app = require('express')();
    app.all('/*', function(req, res) {res.send('process ' + process.pid + ' says hello!').end();})

    var server = app.listen(8000, function() {
        console.log('Process ' + process.pid + ' is listening to all incoming requests');
    });
}